# Desk Turns

A simple script to compute room shifts for working during the Cov19 lockdown in
the Department of Physics at Sapienza University of Rome.

## Usage

```
desk_turns.py [-h] [-r dir] name [name ...]
  name                  Desk names (default: all).

optional arguments:
  -h, --help            show this help message and exit
  -r dir, --rooms-dir dir
                        Directory were the files containing the rooms' specifications are stored
                        (default: current directory).
```

The room specification files are simple JSON files containing
the names of room's occupants and adiacency relations of the desks.
To quickly generate them look at the jupyter notebook `room_graphs_gen.ipynb`

The output of the script is the list of names in each room with the corresponding
turn number. The script tries to minimize the number of shifts and chooses
randomly between equivalent solutions. 